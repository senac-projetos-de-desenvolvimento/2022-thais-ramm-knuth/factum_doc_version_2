# factum_doc

Projeto responsável pela documentação dos subprojetos.

### Sobre o projeto
Detalhes técnicos estão na [Wiki](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_doc_version_2/-/wikis/home) e no repositório da API: [factum_api](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_api).
